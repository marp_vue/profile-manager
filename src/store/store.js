import Vue from 'vue'
import Vuex from 'vuex'
import {generateID} from '../utils'

Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
        profiles: {}
    },
    getters: {
        profiles: (state) => {
            return state.profiles
        }
    },
    mutations: {
        setProfile: (state, {id, def}) => { state.profiles[id] = {id, def} },
        removeProfile: (state, id) => { delete state[id] }
    },
    actions: {
        setProfile: ({ commit }, {id, def}) => {
            if(typeof id == 'undefined' || id ==  null){
                id = generateID()
            }
            commit('setProfile', {id, def})
        },
        removeProfile: ({ commit }, payload) => { commit('removeProfile', payload) }
    }
})