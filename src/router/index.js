import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/Home'
import Profile from '../components/profile'
import ProfileList from '../components/profile/List'
import ProfileEdit from '../components/profile/Edit'
import ProfileDetail from '../components/profile/Detail'
import HTTP404 from '../components/Http404'
import Denied from '../components/Denied'
import Config from '../components/Config'
import Admin from '../components/Admin'

Vue.use(Router)

export default new Router({
  routes: [
    {path: '/', component: Home},
    {
      path: '/user',
      component: Profile,
      children: [
        {path: 'new', name:'userAdd', component: ProfileEdit, props: { isnew: true }},
        {path: 'list', name:'userList', component: ProfileList},
        {path: ':id', name:'userInfo', component: ProfileDetail},
        {path: ':id/edit', name: 'userEdit', component: ProfileEdit}
      ]
    },
    { path: '/users', redirect: {name: 'userList'} },
    { path: '/denied', component: Denied, beforeEnter(to, from, next){ next(false); } },
    { path: '/config', name: 'config', component: Config },
    { path: '/admin', name: 'admin', component: Admin },
    { path: '/404', name: 'HTTP404', component: HTTP404 },
    { path: '*', redirect: {name: 'HTTP404'} }
  ]
})
