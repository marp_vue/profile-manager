import Vue from 'vue'
const eventBus = new Vue()

let _config = {}

function isValid(){
    return Object.keys(_config).length>0
}

export const reader = {
    data(){
        return {
            config: {}
        }
    },
    created(){
        eventBus.$on('config_set', (args) => {
            this.config = {..._config, ...args}
        })
    }
}

export const manager = {
    methods: {
        setConfig: function(args){
            eventBus.$emit('config_set', args)
        },
        isValid: function(){
            return isValid()
        }
    },
    mixins: [reader]
}

export const requireConfig = {
    beforeRouteEnter(to,from,next){
        if(isValid()){
            next()
        }else{
            next('/config')
        }
    }    
}

export default reader

eventBus.$on('config_set', (args) => {
    _config = {..._config, ...args}
})
